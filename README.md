# Chronologia



## Présentation

Chronologia est un "**escape cards**" dédié à la découverte de la **datation relative**.

Le jeu se compose d'un jeu de cartes et d'une application web dédiée.

Dans ce jeu, vous incarnez l'assistant de Lyell, l'inventeur des principes de la datation relative. Il a oublié un document important pour sa conférence qui doit débuter très bientôt. Il vous charge donc de retrouver son document dans son cabinet...

_Ce jeu a d'abord été élaboré pour permettre aux élèves de terminale spécialité de réviser les concepts de datation relative vus lors d'une sortie géologique à Crozon.
Mais il a été également testé avec des élèves de collège et de lycée. Il ne présente pas de difficultés particulières, les stratégies de résolution étant nombreuses._


## Les éléments du jeu

### Les cartes à jouer

Le jeu est composé de 48 cartes et  d'un plateau de jeu. Il suffit d'imprimer les pages en recto-verso, puis de découper l'ensemble.

### L'organigramme du jeu

L'organigramme complet du jeu apporte les solutions. Il permet à l'enseignant de guider les élèves pendant le jeu.

### L'application web
Créée à l'aide du logiciel [Twine](https://twinery.org/), l'[application web](https://cdgsvt.github.io/Chronologia2/) est le compagnon indispensable au déroulement du jeu.

Cette application contient :
- des indices associés à chaque carte,
- des codes (appelés codex dans le jeu) pour franchir les différentes étapes du jeu,
- un code final pour terminer la partie.

## Analyse du jeu

Chronologia a fait l'objet d'un [article](https://scape.enepe.fr/chronologia.html) sur le site S'CAPE.





